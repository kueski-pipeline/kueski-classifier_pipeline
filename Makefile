PIPELINE_ROOT=kueski_classifier_pipeline
PIPELINE_FILE=$(PIPELINE_ROOT)/pipeline.py
NAMESPACE=kubeflow
PIPELINE_OUTPUT=pipeline/artifact.yaml
KFP_ENDPOINT = "http://kubeflow.10.0.0.200.nip.io/"
PIPELINE_NAME="Kueski Classifier"

.PHONY: clean

pipeline/setup:
	pip3 install -e . --upgrade --no-cache-dir
	pip3 install kfp-deployer

	mkdir -p pipeline
	touch pipeline/setup


pipeline/unsetup:
	pip3 uninstall -y	kueski-feature-engineering\
						kueski-balancer\
						kueski-training\
						postgres-importer-component

	rm -f pipeline/setup


pipeline/refresh-env: pipeline/unsetup pipeline/setup
	@echo "Environment refreshed"

$(PIPELINE_OUTPUT): pipeline/setup
	python3 $(PIPELINE_FILE)

	mkdir -p pipeline
	touch pipeline/build


pipeline/upload: $(PIPELINE_OUTPUT)
	kfp-deploy $(KFP_ENDPOINT) $(PIPELINE_NAME) $(PIPELINE_OUTPUT)


clean:
	rm -rf pipeline


clean-all: clean pipeline/unsetup