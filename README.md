# Kueski Pipeline

This is a Kubeflow pipeline to orchestrate the steps of the training. Let's discusse each step with the corresponding repository.

![Pipeline](resources/pipeline.png "The corresponding Kubeflow pipeline.")

The component asks for the following parameters (see the image in the form):

![Parameters](resources/parameters.png "The corresponding Kubeflow pipeline paraemters form")

The paraemters may be easily understandable on each repository README.
## The ideas

* I created a kubeflow pipeline, to run that you need kubeflow installed in a kuberentes cluster. I used minio as an object storage, but in production, it should use s3, gs or az.
* I used object storage instead of local storage or any other storage to allow jobs be implemented in any technology compatible with this technology.
* The jobs are implemented in spark to allow dealing large datasets (pandas is limited to node memory)
* To serve only the last features, the feature engineering job create both, the data for training and the data for serving, with only the latest client data.
* The model is saved as onnx, it avoid the need to keep python joblib or scala/java as the only techonologies able to deploy it.
* The selected intermediate format was JSON, there should be better formats like parquet or avro, but JSON is enough to keep a transposable schema able to be used in the training.
* Instead of a single system and a single repository, in the name of code maintanability, I created many smaller repositories for different jobs and kubeflow componentes.

## Pipeline components
### Run feature engineering

This step runs the feature engineering as described in the first jupyter notebook. The example uses pandas, but this pipeline is implemented on top of spark beeing able to deal large datasets in small memory nodes.

The repository for the application is:
> https://gitlab.com/kueski-pipeline/feature-engineering

The repository for the kubeflow component is:
> https://gitlab.com/kueski-pipeline/feature-engineering_component

This job receives the CSV file from a path in an objct storage or local path (the kubeflow pipeline is configured for minio, but the job can deal with any object storage or local path), and outputs some JSON lines files, a group to be used in the training branch of the pipeline, another (containing only the latest data for clients) to be uploaded into a postgres database for the serving API.

### Load data into postgres for data serving API

This component loads data from local file or object storage (in the pipeline, it is minio), to a table on a postgres database. The database here is intended to be used on the feature and machine learning api.

The repository for the job code is
> https://gitlab.com/kueski-pipeline/postgres-importer

The repository for the kubeflow component is:
> https://gitlab.com/kueski-pipeline/postgres-importer_component

### Split and balance

Here I used spark again to deal large datasets. The data is splited into training and test randomly, then, the training data is balanced using approximated SMOTE, the test data is balanced, but the balanced and non-balanced are saved.

The data types are converted to become float32, used in training and assembled into a vector of features.

The repository for the job code is
> https://gitlab.com/kueski-pipeline/balancer

The repository for the kubeflow component is:
> https://gitlab.com/kueski-pipeline/balancer_component

### Train and validate model

Here the model is trained from the training data generated in the **split and balance** step. The unbalanced validation is used to validated the data. You can provide some minimum values for metrics, if the resulting metrics are enough, the model is saved as **onnx**, to turn it deployable in other technologies, not only Python and Scala/Java.

If the resulting metrics are not good enough, the training fail, breaking the pipeline and ignoring the docker image build/push step.

The repository for the job code is
> https://gitlab.com/kueski-pipeline/training

The repository for the kubeflow component is:
> https://gitlab.com/kueski-pipeline/training_component

### Build image on K8s

This job has only the kubeflow component code because it uses Kaniko to build the docker image from a git repository, object storage bucket, or any other Kaniko compatible context.

It passes MODEL_PATH the parameter as a build argument, then it can generate the model serving docker image with the model trained in this pipeline.

The repository for the kubeflow component is:
> https://gitlab.com/kueski-pipeline/balancer_component


## The serving API

To improve performance, the API is built in RUST. I made a single API to serve features from id and serve the model. There are two docker images, one to build the base imagem, without model, and another to be used in the build image step, creating a new image (based on the last), which includes the model (it shuld be used in the context send to Kaniko).

