from kfp import dsl
from kueski_feature_engineering import make_kueski_feature_engineering
from kueski_feature_engineering.constants import DEFAULT_SPARK_MASTER,\
    DEFAULT_MAX_RECORDS_PER_FILE as DEFAULT_MAX_RECORDS_PER_ENGINEERING_FILE

from kueski_balancer import make_kueski_balancer

from kueski_balancer.constants import DEFAULT_TRAIN_VALIDATION_PROPORTION,\
    DEFAULT_MAX_RECORDS_PER_FILE as DEFAULT_MAX_RECORDS_PER_BALANCED_FILE


from kueski_training import make_kueski_training
from image_builder import make_image_builder


from kfp.compiler import Compiler
from kubernetes.client import V1ObjectReference
from postgres_importer_component import make_postgres_importer
from postgres_importer_component.constants import DEFAULT_BATCH_SIZE, DEFAULT_DB_PORT, DEFAULT_FILE_TYPE


@dsl.pipeline(name="train-classifier", description="")
def kueski_training_pipeline(
        raw_source_file_path: str = "", base_path: str = "s3a://dev/output", spark_master: str = DEFAULT_SPARK_MASTER,
        max_records_per_feature_engineering_file: int = DEFAULT_MAX_RECORDS_PER_ENGINEERING_FILE,
        max_records_per_balanced_file: int = DEFAULT_MAX_RECORDS_PER_BALANCED_FILE,
        train_validation_split_proportion: float = DEFAULT_TRAIN_VALIDATION_PROPORTION,
        min_area_under_roc: float = 0.0, min_area_under_pr: float = 0.0,
        db_feature_engineering_data_table: str = "feature_engineering_data", context_path: str = "",
        resulting_image_name: str = "kueski.com/credit-risk-classsifier",

        db_host: str = "", db_port: int = DEFAULT_DB_PORT, db_username: str = "",
        db_password: str = "", db_name: str = "", db_schema: str = "",
        db_loading_batch_size: int = DEFAULT_BATCH_SIZE):

    run_base_path = f"{base_path}/{dsl.RUN_ID_PLACEHOLDER}"

    feature_engineering_output_path = f"{run_base_path}/feature-engineering-output"
    serving_output_path = f"{run_base_path}/{dsl.RUN_ID_PLACEHOLDER}/serving-output"

    feature_engineering_component = make_kueski_feature_engineering(
        raw_source_file_path, feature_engineering_output_path, serving_output_path, spark_master,
        max_records_per_feature_engineering_file
    )

    feature_engineering_postgres_importer = make_postgres_importer(
        DEFAULT_FILE_TYPE, serving_output_path, db_host, db_port, db_username, db_password, db_name,
        db_schema,
        db_feature_engineering_data_table, db_loading_batch_size)
    feature_engineering_postgres_importer.set_display_name("Load data into postgres for data serving API")
    feature_engineering_postgres_importer.after(feature_engineering_component)

    training_data_folder = f"{run_base_path}/training_data"
    balanced_validation_folder = f"{run_base_path}/validation/balanced"
    unbalanced_validation_folder = f"{run_base_path}/validation/unbalanced"

    balancer_component = make_kueski_balancer(
        feature_engineering_output_path, training_data_folder, balanced_validation_folder,
        unbalanced_validation_folder, train_validation_split_proportion, spark_master, max_records_per_balanced_file)

    balancer_component.after(feature_engineering_component)

    model_output_path = f"{run_base_path}/model.onnx"
    training_component = make_kueski_training(training_data_folder, unbalanced_validation_folder, model_output_path,
                                              spark_master, min_area_under_roc, min_area_under_pr)

    training_component.after(balancer_component)
    image_builder_component = make_image_builder(context_path, model_output_path, resulting_image_name,
                                                 resulting_image_tag=dsl.RUN_ID_PLACEHOLDER)

    image_builder_component.after(training_component)


if __name__ == '__main__':
    pipeline_conf = dsl.PipelineConf()
    pipeline_conf.set_image_pull_secrets([V1ObjectReference(name="registries")])

    Compiler().compile(kueski_training_pipeline, "pipeline/artifact.yaml", pipeline_conf=pipeline_conf)
