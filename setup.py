from setuptools import setup, find_packages

setup(
    name='kueski-classifier_pipelines',
    version='1.0.0',
    packages=find_packages(),
    url='',
    license='',
    author='',
    author_email='',
    description='',
    install_requires=[
        "kfp==1.4.0",
        "kueski-feature-engineering @ git+ssh://git@gitlab.com/kueski-pipeline/feature-engineering_component",
        "kueski-balancer @ git+ssh://git@gitlab.com/kueski-pipeline/balancer_component",
        "kueski-training @ git+ssh://git@gitlab.com/kueski-pipeline/training_component",
        "postgres-importer-component @ git+ssh://git@gitlab.com/kueski-pipeline/postgres-importer_component",
        "image-builder @ git+ssh://git@gitlab.com/kueski-pipeline/image-build_component"
    ]
)
